public class Main {
    public static void main(String[] args) {
        int A = 6, B = 6;
        char arrey[][] = new char[A][B];
        char black = 'Ч', white = 'Б';
        for (int i = 0; i < arrey.length; i++) {
            for (int j = 0; j < arrey.length; j++) {
                if ((i + j) % 2 == 0) {
                    arrey[i][j] = white;
                }
                else {
                    arrey[i][j] = black;
                }
                System.out.print(arrey[i][j] + "\t");
            }
            System.out.print("\n");
        }
    }
}


